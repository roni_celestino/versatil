import { VersatilPage } from './app.po';

describe('versatil App', () => {
  let page: VersatilPage;

  beforeEach(() => {
    page = new VersatilPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
