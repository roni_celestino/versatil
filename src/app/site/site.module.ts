import { SiteRoutingModule } from './site.routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { SwiperModule } from 'angular2-useful-swiper';

import { AngularMaterialModule } from './../angular-material/angular-material.module';


import { SiteComponent } from './site.component';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './component/menu/menu.component';
import { FooterComponent } from './component/footer/footer.component';
import { SobreComponent } from './sobre/sobre.component';
import { EstruturaComponent } from './estrutura/estrutura.component';
import { MatriculaComponent } from './matricula/matricula.component';
import { ContatoComponent } from './contato/contato.component';
import { SlideshowComponent } from './component/slideshow/slideshow.component';
import { ColecaoComponent, ColecaoDetalhesComponent } from './colecao/colecao.component';

import { ProdutoDestaqueComponent } from './component/produto-destaque/produto-destaque.component';


@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    SwiperModule,
    AngularMaterialModule,
    SiteRoutingModule
  ],
  declarations: [
    SiteComponent,
    HomeComponent,
    MenuComponent,
    FooterComponent,
    SobreComponent,
    EstruturaComponent,
    MatriculaComponent,
    ContatoComponent,
    SlideshowComponent,
    ColecaoComponent,
    ProdutoDestaqueComponent,
    ColecaoDetalhesComponent
  ],
  entryComponents: [
    ColecaoDetalhesComponent
  ]
})
export class SiteModule { }
