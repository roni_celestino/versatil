import { Component, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';

@Component({
  selector: 'app-colecao',
  templateUrl: './colecao.component.html',
  styleUrls: ['./colecao.component.css']
})
export class ColecaoComponent implements OnInit {

  constructor(public dialog: MdDialog) { }

  ngOnInit() {
  }

  openDialog() {
    this.dialog.open(ColecaoDetalhesComponent);
  }
}

@Component({
  selector: 'app-colecao-detalhes',
  templateUrl: './colecao-detalhes.component.html',
})

export class ColecaoDetalhesComponent { }
