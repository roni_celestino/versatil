import { NgModule, ModuleWithProviders } from '@angular/core';
import {
  Routes,
  RouterModule,
  CanActivateChild,
  CanDeactivate
} from '@angular/router';

//  COMPONENTES
import { SiteComponent } from './site.component';
import { HomeComponent } from './home/home.component';
import { SobreComponent } from './sobre/sobre.component';
import { EstruturaComponent } from './estrutura/estrutura.component';
import { MatriculaComponent } from './matricula/matricula.component';
import { ContatoComponent } from './contato/contato.component';
import { ColecaoComponent } from './colecao/colecao.component';


const siteRoutes: Routes = [
  {
    path: '', component: SiteComponent, children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'sobre', component: SobreComponent },
      { path: 'colecao', component: ColecaoComponent },
      { path: 'estrutura', component: MatriculaComponent },
      { path: 'contato', component: ContatoComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(siteRoutes)
  ],
  exports: [
    RouterModule
  ]

})

export class SiteRoutingModule { }
