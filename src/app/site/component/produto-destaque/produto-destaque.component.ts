import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-produto-destaque',
  templateUrl: './produto-destaque.component.html',
  styleUrls: ['./produto-destaque.component.css']
})
export class ProdutoDestaqueComponent implements OnInit {
  images: string[];
  config: any = {
    // pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
   // autoplay: 3500,
    freeMode: true,
    slidesPerView: 4,
    spaceBetween: 50,
    loop: true,
    breakpoints: {
      1024: {
        slidesPerView: 4,
        spaceBetween: 40
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 30
      },
      640: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      360: {
        slidesPerView: 1,
        spaceBetween: 10
      }
    }

  };
  constructor() { }

  ngOnInit() {
  }

}
